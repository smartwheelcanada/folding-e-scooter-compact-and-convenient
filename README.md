The Folding E Scooter is a revolutionary transport device that is designed to be compact and lightweight. It has a convenient folding design that can be easily stored in a backpack or bag. The scooter has a powerful 350W motor that can reach speeds of up to 24km/h.

The Folding E Scooter is a lightweight, portable, and foldable electric scooter. The Folding E Scooter is an electric scooter that can be folded up and carried around. The motorized scooter weighs only 18 pounds and has a range of about 15 miles. It costs $1,099 to pre-order one now, but it will retail for $1,299 when it's released in 2020.

It's perfect for people who want to travel with their bike or don't have space for a car or bike because the scooters are compact enough to fit inside your car trunk or closet.

The Folding E Scooter is a new and innovative design that folds into a compact size, making it easy to carry and store. It has a range of around 15 miles on a single charge and can reach speeds up to 15 MPH. The Folding E Scooter is perfect for commuters looking for an alternative to walking or biking, or anyone who's looking for an eco-friendly way to get around town.

The Folding E Scooter is a revolutionary product that has the potential to change the way we travel. The scooter folds in half, making it easy to store and transport.

The Folding E Scooter is an innovative design that solves the problem of carrying around bulky electric scooters. This new design makes it possible for people to take their scooters with them everywhere they go without having to worry about storage space or transportation costs.

Click here for more information... [E-Bikes For Sale](https://www.smartwheel.ca/shop-category/e-bikes) .
